const { app, BrowserWindow, session } = require('electron');
app.once('ready', async () => {
    const
        window = new BrowserWindow({ show: false }),
        window2 = new BrowserWindow({ show: false });
    window.setMenuBarVisibility(false);
    window2.setMenuBarVisibility(false);
    const
        { webContents } = window,
        { defaultSession } = session;
    webContents.on('did-finish-load', () => {
        webContents.insertCSS(`
            [role="presentation"] {
                display: none;
            }
        `);
        window.show();
    });
    defaultSession.webRequest.onCompleted({ urls: ['*://*/*'] }, async request => {
        try {
            const sessionId = request.responseHeaders['set-cookie'].find(item => item.startsWith('sessionid')).split('=')[1].split(';')[0];
            if(sessionId){
                window.close();
                await window2.loadURL(`data:text/html;charset=utf-8,<body style="background-color: black"><span style="color: white"><span style="font-family: sans-serif">${sessionId}</span></span></body>`);
                await window2.show();
            }
        } catch(_){}
    });
    await defaultSession.clearStorageData();
    await defaultSession.clearCache();
    await window.loadURL('https://www.instagram.com/accounts/login/');
});